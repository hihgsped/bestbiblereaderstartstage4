package bibleReader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @author Carl Best
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object (WRONG) constructed from the file bibleFile, or null
	 *         if there was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {
		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(bibleFile));
			VerseList verses = new VerseList("unknown", "");
			String titleLine = br.readLine();
			String title;
			String desc;
			if (titleLine.equals("")) {
				return verses;
			}
			else if (! titleLine.contains(": ")) {
				title = titleLine;
				desc = "";
			} else {
				String[] splitTitle = titleLine.split(": ");
				title = splitTitle[0];
				desc = splitTitle[1];
			}
			verses = new VerseList(title, desc);

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.contains("@")) {
					return null;
				}
				String[] verseData = line.split("@");
				String bookAbbr = verseData[0];
				String chapVerse = verseData[1];
				String text = verseData[2];
				if (!chapVerse.contains(":")) {
					return null;
				}
				String[] refData = chapVerse.split(":");
				int chapNum = Integer.parseInt(refData[0]);
				int verseNum = Integer.parseInt(refData[1]);
				BookOfBible book = BookOfBible.getBookOfBible(bookAbbr);
				if (book == null) {
					return null;
				}
				Reference verseRef = new Reference(book, chapNum, verseNum);
				verses.add(new Verse(verseRef, text));
			}
			br.close();
			return verses;
		} catch (IOException e) {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
			System.out.println("stopping bible loading");
		}
		return null;
	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * In XMV formatting, each line begins with a tag surrounded by <>.
	 * The first word in each tag denotes what information is stored on
	 * that line. XMV files begin with <Version> tags, then define a <Book>,
	 * then a <Chapter>, and finally a series of <Verse>s to go with them.
	 * 
	 * @param bibleFile The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {

		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(bibleFile));
			VerseList verses = new VerseList("unknown", "");
			BookOfBible currentBook = null;
			int currentChap = 0;
			String line;
			while ((line = br.readLine()) != null) {
				String[] lineSplit = line.split(">");
				String[] tagSplit = lineSplit[0].split(" ", 2);
				String tag = tagSplit[0];
				if (tag.equals("<Version")) {
					String[] versionSplit = tagSplit[1].split(": ");
					String version = versionSplit[0];
					String description = versionSplit[1] + ">"; // why even have the closing carat at the end?
					verses = new VerseList(version, description);
				}
				else if (tag.equals("<Book")) {
					String[] bookSplit = tagSplit[1].split(", ");
					String bookName = bookSplit[0].trim();
					currentBook = BookOfBible.getBookOfBible(bookName);
				}
				else if (tag.equals("<Chapter")) {
					currentChap = Integer.parseInt(tagSplit[1].trim());
				}
				else if (tag.equals("<Verse")) {
					int currentVerse = Integer.parseInt(tagSplit[1].trim());
					Verse v = new Verse(currentBook, currentChap, currentVerse, lineSplit[1].trim());
					verses.add(v); // nullpointerexception here means there was no version tag line
				}
			}
			br.close();
			return verses;
		} catch (IOException e) {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
			System.out.println("stopping bible loading");
		}
		
		return null;

	}

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */

	public static void writeBibleATV(File file, Bible bible) {		
		writeVersesATV(file, bible.getVersion() + ": " + bible.getTitle(), bible.getAllVerses());
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {

		
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			bw.write(description + "\n");
			
			for (Verse v : verses) {
				Reference r = v.getReference();
				String book = r.getBook().replaceAll(" ", "").toLowerCase().trim();
				String toWrite = book + "@" + r.getChapter() + ":" + r.getVerse() + "@" + v.getText().trim();
				bw.write(toWrite + "\n");
			}
			
			bw.close();
		} catch (IOException e) {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {

		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));
			
			bw.write(text);
			
			bw.close();
		} catch (IOException e) {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
		}
	}
}
