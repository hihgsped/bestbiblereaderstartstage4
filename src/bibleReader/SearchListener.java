package bibleReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;

import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;

/**
 * Listener for the search button.
 * 
 * @author Carl Best
 * @author Andrew Arent
 */

public class SearchListener implements ActionListener {
	private ResultView results;
	private JTextField search;
	public SearchListener(ResultView results, JTextField search) {
		this.results = results;
		this.search = search;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String in = search.getText();
		if (in.equals("")) {
			results.setSummary("Enter a search into the search bar above.");
			return;
		}
		ArrayList<Reference> refs = this.results.getSearchRefs(in);
		results.in = in;
		results.setSummary(refs.size() + " results were found containing search " + in);
		
		this.results.setRefs(new NavigableResults(refs, in, ResultType.SEARCH));

		this.results.updateResultText();
	}
}