package bibleReader;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.html.StyleSheet;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;
import bibleReader.model.TreeMapBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @author Carl Best
 * @author Andrew Arent
 */
@SuppressWarnings("serial")
public class BibleReaderApp extends JFrame {
	// Change these to suit your needs.
	public static final int width = 600;
	public static final int height = 600;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;

	private JPanel enterView;
	public JButton enterSearchButton;
	public JButton enterPassageButton;
	public JTextField enterSearchText;

	public ResultView resultView;

	/**
	 * Default constructor. We may want to replace this with a different one.
	 */
	public BibleReaderApp() {
		// There is no guarantee that this complete/correct, so take a close
		// look to make sure you understand what this code is doing in case
		// you need to modify or add to it.
		model = new BibleReaderModel(); // For now call the default constructor. This might change.
		File kjvFile = new File("kjv.atv");
		File esvFile = new File("esv.atv");
		File asvFile = new File("asv.xmv");

		Bible kjv = new ArrayListBible(BibleIO.readBible(kjvFile));
		Bible esv = new ArrayListBible(BibleIO.readBible(esvFile));
		Bible asv = new ArrayListBible(BibleIO.readBible(asvFile));

		model.addBible(kjv);
		model.addBible(esv);
		model.addBible(asv);

		resultView = new ResultView(model);

		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Set up the main GUI. Make sure you don't forget to put resultView somewhere!
	 */
	private void setupGUI() {		
		this.setLayout(new BorderLayout());

		this.enterView = new JPanel(new FlowLayout());
		this.enterSearchText = new JTextField(20);
		enterSearchText.setName("InputTextField");
		this.enterSearchButton = new JButton("Search by word");
		enterSearchButton.setName("SearchButton");
		this.enterPassageButton = new JButton("Search by passage");
		enterPassageButton.setName("PassageButton");

		this.resultView = new ResultView(this.model);

		this.add(this.enterView, BorderLayout.NORTH);
		this.enterView.add(this.enterSearchText);
		this.enterView.add(this.enterSearchButton);
		this.enterView.add(this.enterPassageButton);

		this.add(this.resultView, BorderLayout.CENTER);
		
		this.enterSearchText.addActionListener(new SearchListener(this.resultView, this.enterSearchText));
		this.enterSearchButton.addActionListener(new SearchListener(this.resultView, this.enterSearchText));
		this.enterPassageButton.addActionListener(new PassageListener(this.resultView, this.enterSearchText));

		JMenu fileMenu = new JMenu("File");
		
		JMenuItem exitMenuItem = new JMenuItem("Exit");
		exitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(exitMenuItem);
		
		JMenu helpMenu = new JMenu("Help");
		JMenuItem aboutMenuItem = new JMenuItem("About");
		JFrame cheater = this;
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(cheater,
						"This application will give a series of verses based on a user's input. \n" +
						"If a valid bible verse / passage is entered into the text input and the \n" +
						"`Passage` button is hit, it will pull up each passage within the passage \n" +
						"entered. If a string of text is entered into the text input and the \n" +
						"`Enter` button is hit, it will pull up each passage in the entire bible \n" +
						"that contains that string of text.\n\n" + 
						"This app was created by Andrew Arent and Carl Best.");
			}
		});
		helpMenu.add(aboutMenuItem);

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		this.setJMenuBar(menuBar);
		
		// The stage numbers below may change, so make sure to pay attention to
		// what the assignment says.
		// TODO Add passage lookup: Stage ?
		// TODO Add 2nd version on display: Stage ?
		// TODO Limit the displayed search results to 20 at a time: Stage ?
		// TODO Add 3rd versions on display: Stage ?
		// TODO Format results better: Stage ?
		// TODO Display cross references for third version: Stage ?
		// TODO Save/load search results: Stage ?
	}
}
