package bibleReader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;

import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;

/**
 * Listener for the passage button.
 * 
 * @author Carl Best
 * @author Andrew Arent
 */

public class PassageListener implements ActionListener {
	private ResultView results;
	private JTextField search;
	public PassageListener(ResultView results, JTextField search) {
		this.results = results;
		this.search = search;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String in = search.getText();
		if (in.equals("")) {
			results.setSummary("Enter a passage into the search bar above.");
			return;
		}
		ArrayList<Reference> refs = this.results.getPassageRefs(in);
		results.setSummary(refs.size() + " results were found matching passage " + in);
		
		this.results.setRefs(new NavigableResults(refs, in, ResultType.PASSAGE));

		this.results.updateResultText();
	}
}