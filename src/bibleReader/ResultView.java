package bibleReader;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;

/**
 * The display panel for the Bible Reader.
 * 
 * @author Carl Best
 * @author Andrew Arent
 */
public class ResultView extends JPanel {

	private JLabel summary;
	public JEditorPane results;
	public JButton next;
	public JButton prev;
	
	private NavigableResults refs;
	public String in;
	private BibleReaderModel model;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to model to look things up.
	 * 
	 * @param myModel The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		super(new BorderLayout());
		
		this.model = myModel;

		this.summary = new JLabel("Enter a search into the search bar above.");
		this.results = new JEditorPane();
		this.results.setEditable(false);
		this.results.setContentType("text/html");
		this.results.setName("OutputEditorPane");
		JScrollPane resultsPane = new JScrollPane(this.results);
		
		this.add(this.summary, BorderLayout.NORTH);
		this.add(resultsPane, BorderLayout.CENTER);
		
		this.next = new JButton("next");
		this.prev = new JButton("previous");
		this.prev.setEnabled(false);
		this.next.setEnabled(false);
		this.next.setName("NextButton");
		this.prev.setName("PreviousButton");
		this.next.addActionListener(new NextListener());
		this.prev.addActionListener(new PrevListener());
		
		JPanel navButtonContainer = new JPanel(new GridLayout());
		navButtonContainer.add(this.prev);
		navButtonContainer.add(this.next);
		this.add(navButtonContainer, BorderLayout.SOUTH);
	}
	
	public void setRefs(NavigableResults refs) {
		this.refs = refs;
	}
	
	public void setSummary(String summary) {
		this.summary.setText(summary);
	}
	
	public ArrayList<Reference> getPassageRefs(String in) {
		return this.model.getReferencesForPassage(in);
	}
	public ArrayList<Reference> getSearchRefs(String in) {
		return this.model.getReferencesContaining(in);
	}

	public void updateResultText() {
		ArrayList<Reference> refs = this.refs.currentResults();
		if (this.refs.hasNextResults()) {
			this.next.setEnabled(true);
		} else {
			this.next.setEnabled(false);
		}
		if (this.refs.hasPreviousResults()) {
			this.prev.setEnabled(true);
		} else {
			this.prev.setEnabled(false);
		}
		
		HashMap<String, StringBuffer> rows = new HashMap<>();
		rows.put("header", new StringBuffer());
		rows.get("header").append("<html><head>"
		+ "<style>"
	    + "table {"
	    	+ "background-color: black;"
	    + "}"
		+ "td {"
		    + "text-align: left;"
		    + "background-color: white;"
	    + "}"
		+ ".headertd {"
		    + "background-color: gray;"
		    + "color: white;"
		+ "}"
	    + "</style></head>");
		
		rows.get("header").append("<table><th>");
		if (this.refs.getType() == ResultType.SEARCH) { 
			for (Reference ref : refs) {
				rows.get("header").append("<td class=headertd>" + ref.toString() + "</td>");
			}
			rows.get("header").append("</th>");
			
			for (String version : model.getVersions()) {
				rows.put(version, new StringBuffer());
				rows.get(version).append("<tr><td class=headertd>" + version + "</td>");
				
				for (Reference ref : refs) {
					String verse = model.getText(version, ref);
					verse = verse.replaceAll("(?i)" + in, "<b>$0</b>");
					if (! (verse.contains(in))) {
						verse = "<i>" + verse + "</i>";
					}
					rows.get(version).append("<td>" + verse + "</td>");
				}
				rows.get(version).append("</tr>");
			}
		}
		else if (this.refs.getType() == ResultType.PASSAGE) {
			Reference start = refs.get(0);
			Reference end = refs.get(refs.size()-1);
			
			String endString = "" + end;
			if (start.getBook().equals(end.getBook())) {
				if (start.getChapter() == end.getChapter()) {
					endString = "" + end.getVerse();
				}
				else {
					endString = end.getChapter() + ":" + end.getVerse();
				}
			}
			
			rows.get("header").append("<td class=headertd>" + start + "-" + endString + "</td>");
			for (String version : model.getVersions()) {
				rows.put(version, new StringBuffer());
				rows.get(version).append("<tr><td class=headertd>" + version + "</td>");
				int lastChap = refs.get(0).getChapter();
				for (Reference ref : refs) {
					String verse = model.getText(version, ref);
					if (lastChap != ref.getChapter()) {
						rows.get(version).append("<br><br>");
						lastChap = ref.getChapter();
					}
					rows.get(version).append("<sup>" + ref.getVerse() + "</sup>" + verse + " ");
				}
				
				rows.get(version).append("</td></tr>");
			}
		}
		
		StringBuffer text = new StringBuffer();
		text.append(rows.get("header"));
		for (String version : model.getVersions()) {
			text.append(rows.get(version));
		}
		text.append("</table></html>");
		
		this.results.setText(text.toString());
	}
	
	private class NextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			refs.nextResults();
			updateResultText();
		}
	}
	private class PrevListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			refs.previousResults();
			updateResultText();
		}
	}
}
