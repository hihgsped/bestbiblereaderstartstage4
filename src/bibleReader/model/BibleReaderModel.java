package bibleReader.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 * @author Carl Best
 */
public class BibleReaderModel implements MultiBibleModel {

	private HashMap<String, Bible> bibles;

	/**
	 * Default constructor. Instantiates the bibles HashMap.
	 */
	public BibleReaderModel() {
		this.bibles = new HashMap<>();
	}

	@Override
	public String[] getVersions() {
		ArrayList<String> versions = new ArrayList<>(bibles.keySet());
		Collections.sort(versions);
		return versions.toArray(new String[] {});
	}

	@Override
	public int getNumberOfVersions() {
		return bibles.keySet().size();
	}

	@Override
	public void addBible(Bible bible) {
		String version = bible.getVersion();
		this.bibles.put(version, bible);
	}

	@Override
	public Bible getBible(String version) {
		return this.bibles.get(version);
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		TreeSet<Reference> allRefs = new TreeSet<>();

		for (String version : this.bibles.keySet()) {
			Bible bible = this.bibles.get(version);
			allRefs.addAll(bible.getReferencesContaining(words));
		}
		return new ArrayList<Reference>(allRefs);
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		return this.getBible(version).getVerses(references);
	}
	// ---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		Bible bible = this.bibles.get(version);
		if (bible == null) { return ""; }
		String res = bible.getVerseText(reference);
		if (res == null) { return ""; }
		return res;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		System.out.println(reference);
		Pattern bookPattern = Pattern.compile("\\s*((?:1|2|3|I|II|III)\\s*\\w+|(?:\\s*[a-zA-Z]+)+)\\s*(.*)");

		TreeSet<Reference> refs = new TreeSet<>();
		String trimmed = reference.trim();
		Matcher m = bookPattern.matcher(reference);

		if (m.matches()) {
			try {
				// we have at least a book name and a chapter
				System.out.println("at least book and chapter");
				String bookName = m.group(1).trim();
				BookOfBible book = BookOfBible.getBookOfBible(bookName);
				String other = m.group(2).trim();
				if (other.contains("-") && !other.equals("-")) {
					// we have at least two chapters
					System.out.println("at least two chapters");
					String[] pass1pass2 = other.split("-");
					String pass1 = pass1pass2[0].trim();
					String pass2 = pass1pass2[1].trim();
					if (pass1.contains(":") && !pass1.equals(":")) {
						// we have a chapter-verse pair
						System.out.println("at least chapter-verse pair");
						String[] chapterVerse = pass1.split(":");
						int chapter = Integer.parseInt(chapterVerse[0].trim());
						int verse = Integer.parseInt(chapterVerse[1].trim());
						if (pass2.contains(":") && !pass2.equals(":")) {
							// we have two chapter-verse pairs ( ENDING )
							System.out.println("two chapter-verse pairs (ENDING)");
							String[] chapterVerse2 = pass2.split(":");
							int chapter2 = Integer.parseInt(chapterVerse2[0].trim());
							int verse2 = Integer.parseInt(chapterVerse2[1].trim());
							refs.addAll(this.getPassageReferences(new Reference(book, chapter, verse),
									new Reference(book, chapter2, verse2)));
						} else {
							// we have a chapter:verse-verse ( ENDING )
							System.out.println("chapter:verse-verse (ENDING)");
							int verse2 = Integer.parseInt(pass2);
							refs.addAll(this.getPassageReferences(book, chapter, verse, verse2));
						}
					} else if (pass2.contains(":") && !pass2.equals(":")) {
						// we have a chapter:chapter-verse ( ENDING )
						System.out.println("chapter:chapter-verse (ENDING)");
						int chapter = Integer.parseInt(pass1);
						String[] chapterVerse2 = pass2.split(":");
						int chapter2 = Integer.parseInt(chapterVerse2[0]);
						int verse2 = Integer.parseInt(chapterVerse2[1]);
						refs.addAll(this.getPassageReferences(new Reference(book, chapter, 1),
								new Reference(book, chapter2, verse2)));
					} else {
						// we have a chapter-chapter ( ENDING )
						System.out.println("chapter-chapter (ENDING)");
						int chapter = Integer.parseInt(pass1);
						int chapter2 = Integer.parseInt(pass2);
						refs.addAll(this.getChapterReferences(book, chapter, chapter2));
					}
				} else if (!other.equals("")) {
					// we have one chapter
					System.out.println("at least one chapter");
					if (other.contains(":") && !other.equals(":")) {
						// we have one chapter and one verse ( ENDING )
						System.out.println("one chapter one verse (ENDING)");
						String[] chapterVerse = other.split(":");
						int chapter = Integer.parseInt(chapterVerse[0].trim());
						int verse = Integer.parseInt(chapterVerse[1].trim());
						refs.addAll(this.getVerseReferences(book, chapter, verse));
					} else {
						// we only have the chapter ( ENDING )
						System.out.println("only chapter (ENDING)");
						int chapter = Integer.parseInt(other);
						refs.addAll(this.getChapterReferences(book, chapter));
					}
				} else {
					// we only have a book ( ENDING )
					System.out.println("only book (ENDING)");
					refs.addAll(this.getBookReferences(BookOfBible.getBookOfBible(trimmed)));
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		return new ArrayList<Reference>(refs);
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of delegate
	// methods in that they call a method on the Bible class to do most of the work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			Reference r = new Reference(book, chapter, verse);
			if (b.isValid(r)) {
				refs.add(r);
			}
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesInclusive(startVerse, endVerse));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesForBook(book));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesForChapter(book, chapter));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesForChapters(book, chapter1, chapter2));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesForPassage(book, chapter, verse1, verse2));
		}
		return new ArrayList<Reference>(refs);
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		TreeSet<Reference> refs = new TreeSet<>();
		for (String version : this.bibles.keySet()) {
			Bible b = this.bibles.get(version);
			refs.addAll(b.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2));
		}
		return new ArrayList<Reference>(refs);
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}
