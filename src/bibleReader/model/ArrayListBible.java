package bibleReader.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @author Carl Best (provided the implementation)
 */
public class ArrayListBible implements Bible {

	private VerseList verses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses -  All of the verses of this version of the Bible.
	 * @param title - A title for this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
		this.verses = new VerseList(verses);
		//this.verses.add(new Verse(BookOfBible.Dummy, 1, 1, ""));
	}

	@Override
	public int getNumberOfVerses() {
		return this.verses.size();// - 1;
	}

	@Override
	public String getVersion() {
		return this.verses.getVersion();
	}

	@Override
	public String getTitle() {
		return this.verses.getDescription();
	}

	@Override
	public boolean isValid(Reference ref) {
		return this.getVerse(ref) != null;
	}

	@Override
	public String getVerseText(Reference r) {
		Verse v = this.getVerse(r);
		if (v == null) { return null; }
		return v.getText();
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse v : this.verses) {
			if (v.getReference().equals(r)) { return v; }
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		return this.getVerse(new Reference(book, chapter, verse));
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getAllVerses() {
		return new VerseList(this.verses);
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList matches = new VerseList(this.getVersion(), phrase);
		if (phrase.equals("")) { return matches; }
		for (Verse verse : this.verses) {
			if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
				matches.add(verse);
			}
		}
		return matches;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> matches = new ArrayList<>();
		if (phrase.equals("")) { return matches; }
		for (Verse verse : this.verses) {
			if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
				matches.add(verse.getReference());
			}
		}
		return matches;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList matches = new VerseList(this.getVersion(), "Arbitrary list of Verses");
		for (Reference ref : references) {
			matches.add(this.getVerse(ref));
		}
		return matches;
	}
	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		ArrayList<Reference> refs = this.getReferencesForChapter(book, chapter);
		return refs.get(refs.size()-1).getVerse();
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		ArrayList<Reference> refs = this.getReferencesForBook(book);
		return refs.get(refs.size()-1).getChapter();
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! (this.isValid(firstVerse) && this.isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0)) { return refs; }
		
		for (Verse v : this.verses) {
			Reference ref = v.getReference();
			if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0) {
				refs.add(ref);
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! (this.isValid(firstVerse) && firstVerse.compareTo(lastVerse) < 0)) { return refs; }
		
		for (Verse v : this.verses) {
			Reference ref = v.getReference();
			if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0) {
				refs.add(ref);
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! this.isValid(new Reference(book, 1, 1))) { return refs; };
		
		for (Verse v : this.verses) {
			if (v.getReference().getBookOfBible().equals(book)) {
				refs.add(v.getReference());
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! this.isValid(new Reference(book, chapter, 1))) { return refs; }
		
		for (Verse v : this.verses) {
			Reference r = v.getReference();
			if (r.getBookOfBible().equals(book) && r.getChapter() == chapter) {
				refs.add(v.getReference());
			}
		}
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2, 1);
		if (! (this.isValid(ref1) && this.isValid(ref2) && ref1.compareTo(ref2) < 0)) { return new ArrayList<Reference>(); }
		
		ref2 = new Reference(book, chapter2, this.getLastVerseNumber(book, chapter2));
		ArrayList<Reference> refs = this.getReferencesInclusive(ref1, ref2);
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		if (! (this.isValid(ref1) && this.isValid(ref2) && ref1.compareTo(ref2) < 0)) { return new ArrayList<Reference>(); }
		
		ArrayList<Reference> refs = this.getReferencesInclusive(ref1, ref2);
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);
		if (! (this.isValid(ref1) && this.isValid(ref2))) { return new ArrayList<Reference>(); }
		
		ArrayList<Reference> refs = this.getReferencesInclusive(ref1, ref2);
		return refs;
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.getVersion(), "verses from " + firstVerse + " through " + lastVerse);
		if (! (this.isValid(firstVerse) && this.isValid(lastVerse) && firstVerse.compareTo(lastVerse) <= 0)) { return verses; }
		
		for (Verse v : this.verses) {
			Reference ref = v.getReference();
			if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) <= 0) {
				verses.add(v);
			}
		}
		return verses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.getVersion(), "verses from " + firstVerse + " until (excluding) " + lastVerse);
		if (! (this.isValid(firstVerse) && this.isValid(lastVerse))) { return verses; }
		
		for (Verse v : this.verses) {
			Reference ref = v.getReference();
			if (ref.compareTo(firstVerse) >= 0 && ref.compareTo(lastVerse) < 0) {
				verses.add(v);
			}
		}
		return verses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book == null) { return new VerseList(this.getVersion(), null); }
		VerseList verses = new VerseList(this.getVersion(), book.toString());
		if (! this.isValid(new Reference(book, 1, 1))) { return verses; };
		
		for (Verse v : this.verses) {
			if (v.getReference().getBookOfBible().equals(book)) {
				verses.add(v);
			}
		}
		return verses;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter);
		if (! this.isValid(new Reference(book, chapter, 1))) { return verses; }
		
		for (Verse v : this.verses) {
			Reference r = v.getReference();
			if (r.getBookOfBible().equals(book) && r.getChapter() == chapter) {
				verses.add(v);
			}
		}
		return verses;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter1 + "-" + chapter2);
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2, 1);
		if (! (this.isValid(ref1) && this.isValid(ref2) && ref1.compareTo(ref2) < 0)) { return verses; }
		
		ref2 = new Reference(book, chapter2, this.getLastVerseNumber(book, chapter2));
		for (Verse v : this.getVersesInclusive(ref1, ref2)) {
			verses.add(v);
		}
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter + ":" + verse1 + "-" + verse2);
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		if (! (this.isValid(ref1) && this.isValid(ref2) && ref1.compareTo(ref2) < 0)) { return verses; }
		
		for (Verse v : this.getVersesInclusive(ref1, ref2)) {
			verses.add(v);
		}
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		VerseList verses = new VerseList(this.getVersion(), book.toString() + " " + chapter1 + ":" + verse1 + "-" + chapter2 + ":" + verse2);
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);
		if (! (this.isValid(ref1) && this.isValid(ref2) && ref1.compareTo(ref2) < 0)) { return verses; }
		
		for (Verse v : this.getVersesInclusive(ref1, ref2)) {
			verses.add(v);
		}
		return verses;
	}
}
