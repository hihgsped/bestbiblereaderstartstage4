package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author ? (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String						version;
	private String                      description;
	private TreeMap<Reference, Verse>	verses;

	// Or replace the above with:
	// private TreeMap<Reference, Verse> theVerses;
	// Add more fields as necessary.

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		this.verses = new TreeMap<>();
		this.version = verses.getVersion();
		this.description = verses.getDescription();
		for (Verse v : verses) {
			this.verses.put(v.getReference(), v);
		}
	}

	@Override
	public int getNumberOfVerses() {
		return this.verses.size();
	}

	@Override
	public VerseList getAllVerses() {
		VerseList allVerses = new VerseList(this.version, this.description);
		for (Reference ref : this.verses.keySet()) {
			allVerses.add(this.getVerse(ref));
		}
		return allVerses;
	}

	@Override
	public String getVersion() {
		return this.version;
	}

	@Override
	public String getTitle() {
		return this.description;
	}

	@Override
	public boolean isValid(Reference ref) {
		return this.getVerse(ref) != null;
	}

	@Override
	public String getVerseText(Reference r) {
		Verse v = this.getVerse(r);
		if (v == null) { return null; }
		return v.getText();
	}

	@Override
	public Verse getVerse(Reference r) {
		if (r == null || r.getBookOfBible() == null) {
			return null;
		}
		return this.verses.get(r);
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		return this.getVerse(new Reference(book, chapter, verse));
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	//
	// For Stage 11 the first two methods below will be implemented as specified in the comments.
	// Do not over think these methods. All three should be pretty straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they work better.
	// At that stage you will create another class to facilitate searching and use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList matches = new VerseList(this.getVersion(), phrase);
		if (phrase.equals("")) { return matches; }
		
		for (Reference r : this.verses.keySet()) {
			Verse v = this.getVerse(r);
			if (v.getText().toLowerCase().contains(phrase.toLowerCase())) {
				matches.add(v);
			}
		}
		
		return matches;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> matches = new ArrayList<>();
		if (phrase.equals("")) { return matches; }
		phrase = phrase.toLowerCase();
		
		for (Reference r : this.verses.keySet()) {
			Verse v = this.getVerse(r);
			if (v.getText().toLowerCase().contains(phrase)) {
				matches.add(r);
			}
		}
		
		return matches;
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList matches = new VerseList(this.version, "Arbitrary list of Verses");
		
		for (Reference r : references) {
			matches.add(this.getVerse(r));
		}
		
		return matches;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 11.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		int verseNumber = -1;
		if (book == null) { return verseNumber; }

		Reference current = new Reference(book, chapter, 1);
		
		while (current != null && current.getChapter() == chapter) {
			verseNumber = current.getVerse();
			current = this.verses.higherKey(current);
		}
		
		return verseNumber;
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		int chapterNumber = -1;
		if (book == null) { return chapterNumber; }

		Reference current = new Reference(book, 1, 1);
		
		while (this.isValid(current)) {
			chapterNumber = current.getChapter();
			current = new Reference(book, current.getChapter() + 1, 1);
		}
		
		return chapterNumber;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! this.isValid(firstVerse) || ! this.isValid(lastVerse) || firstVerse.compareTo(lastVerse) > 0) {
			return refs;
		}

		Reference realLast = this.verses.higherKey(lastVerse);
		Reference current = firstVerse;
		
		while (current != null && ! current.equals(realLast)) {
			refs.add(current);
			current = this.verses.higherKey(current);
		}
		
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> refs = new ArrayList<>();
		if (! this.isValid(firstVerse) || ! this.isValid(lastVerse) || firstVerse.compareTo(lastVerse) > 0) {
			return refs;
		}
		
		Reference current = firstVerse;
		while (current != null && ! current.equals(lastVerse)) {
			refs.add(current);
			current = this.verses.higherKey(current);
		}
		
		return refs;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		int lastChapter = this.getLastChapterNumber(book);
		int lastVerse = this.getLastVerseNumber(book, lastChapter);
		
		Reference start = new Reference(book, 1, 1);
		Reference end = new Reference(book, lastChapter, lastVerse);
				
		return this.getReferencesInclusive(start, end);
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		int lastVerse = this.getLastVerseNumber(book, chapter);
		
		Reference start = new Reference(book, chapter, 1);
		Reference end = new Reference(book, chapter, lastVerse);
				
		return this.getReferencesInclusive(start, end);
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		int lastVerse = this.getLastVerseNumber(book, chapter2);
		
		Reference start = new Reference(book, chapter1, 1);
		Reference end = new Reference(book, chapter2, lastVerse);
				
		return this.getReferencesInclusive(start, end);
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference start = new Reference(book, chapter, verse1);
		Reference end = new Reference(book, chapter, verse2);
		
		return this.getReferencesInclusive(start, end);
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference start = new Reference(book, chapter1, verse1);
		Reference end = new Reference(book, chapter2, verse2);
		
		return this.getReferencesInclusive(start, end);
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.version, firstVerse + " - " + lastVerse);
		
		for (Reference r : this.getReferencesInclusive(firstVerse, lastVerse)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList verses = new VerseList(this.version, firstVerse + " - " + lastVerse + " excluding the final one");
		
		for (Reference r : this.getReferencesExclusive(firstVerse, lastVerse)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book == null) { return new VerseList(this.version, "u stoopid"); }
		VerseList verses = new VerseList(this.version, book.toString());
		
		for (Reference r : this.getReferencesForBook(book)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		VerseList verses = new VerseList(this.version, book + " " + chapter);
		
		for (Reference r : this.getReferencesForChapter(book, chapter)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		VerseList verses = new VerseList(this.version, book + " " + chapter1 + " - " + chapter2);
		
		for (Reference r : this.getReferencesForChapters(book, chapter1, chapter2)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		VerseList verses = new VerseList(this.version, book + " " + chapter + ":" + verse1 + " - " + verse2);
		
		for (Reference r : this.getReferencesForPassage(book, chapter, verse1, verse2)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		VerseList verses = new VerseList(this.version, book + " " + chapter1 + ":" + verse1 + " - " + chapter2 + ":" + verse2);
		
		for (Reference r : this.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2)) {
			verses.add(this.getVerse(r));
		}
		
		return verses;
	}

}
