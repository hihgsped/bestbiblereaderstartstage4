package bibleReader.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bibleReader.BibleIO;
import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.VerseList;

/**
 * 
 * @author Carl Best
 * @author Andrew Arent
 */
public class TestGetReferencesForPassage {
	private static VerseList versesFromFile;
	private BibleReaderModel model;
	
	@BeforeClass
	public static void readFile() {
		// Our tests will be based on the KJV version for now.
		File file = new File("kjv.atv");
		// We read the file here so it isn't done before every test.
		versesFromFile = BibleIO.readBible(file);
	}
	
	@Before
	public void setup() {
		Bible bible = new ArrayListBible(versesFromFile);
		this.model = new BibleReaderModel();
		this.model.addBible(bible);
	}
	
	@Test
	public void test_getReferencesForPassage_singleTarget() {
		ArrayList<Reference> refs = new ArrayList<>();
		refs.addAll(this.model.getReferencesForPassage("John 3 : 16"));
		refs.addAll(this.model.getReferencesForPassage("Gen 1:1"));
		refs.addAll(this.model.getReferencesForPassage("Revelation 22:21"));
		
		ArrayList<Reference> expected = new ArrayList<Reference>();
		expected.add(new Reference(BookOfBible.John, 3, 16));
		expected.add(new Reference(BookOfBible.Genesis, 1, 1));
		expected.add(new Reference(BookOfBible.Revelation, 22, 21));
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_singleChapter() {
		ArrayList<Reference> refs = new ArrayList<>();
		refs.addAll(this.model.getReferencesForPassage(" Ecclesiastes 3 : 1 - 8 "));
		refs.addAll(this.model.getReferencesForPassage("Joshua 24:28-33"));
		refs.addAll(this.model.getReferencesForPassage("Psalm 23:1-6"));
		
		ArrayList<Reference> expected = new ArrayList<Reference>();
		for (int i = 1; i < 9; i ++) {
			expected.add(new Reference(BookOfBible.Ecclesiastes, 3, i));
		}
		for (int i = 28; i < 34; i ++) {
			expected.add(new Reference(BookOfBible.Joshua, 24, i));
		}
		for (int i = 1; i < 7; i ++) {
			expected.add(new Reference(BookOfBible.Psalms, 23, i));
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_multipleChapters() {
		// Song of Solomon 3
		ArrayList<Reference> refs = this.model.getReferencesForPassage("Song of Solomon 3");
		ArrayList<Reference> expected = new ArrayList<Reference>();
		
		for (int i = 1; i < 12; i ++) {
			expected.add(new Reference(BookOfBible.SongOfSolomon, 3, i));
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		// Revelation 22
		refs = this.model.getReferencesForPassage("Revelation 22");
		expected = new ArrayList<Reference>();
		
		for (int i = 1; i < 22; i ++) {
			expected.add(new Reference(BookOfBible.Revelation, 22, i));
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		// 1 Tim 2-4
		refs = this.model.getReferencesForPassage("1 Tim 2-4");
		expected = new ArrayList<Reference>();
		
		int[] verseCounts = new int[] { 15, 16, 16 };
		int chap = 2;
		for (int verseCount : verseCounts) {
			for (int verse = 1; verse < (verseCount + 1); verse ++) {
				expected.add(new Reference(BookOfBible.Timothy1, chap, verse));
			}
			chap ++;
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		// 1 John 2-3
		refs = this.model.getReferencesForPassage("1 John 2 - 3");
		expected = new ArrayList<Reference>();
		
		verseCounts = new int[] { 29, 24 };
		chap = 2;
		for (int verseCount : verseCounts) {
			for (int verse = 1; verse < verseCount+1; verse ++) {
				expected.add(new Reference(BookOfBible.John1, chap, verse));
			}
			chap ++;
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_multipleChaptersSpecifiedVerse() {
		// Isa 52:13 - 52:12
		ArrayList<Reference> refs = this.model.getReferencesForPassage("Isa 52 : 13 - 53 : 12");
		ArrayList<Reference> expected = new ArrayList<Reference>();
		
		int[] verseCounts = new int[] { 15, 12 };
		int[] startAtVerses = new int[] { 13, 1 };
		int chap = 52;
		for (int index = 0; index < verseCounts.length; index ++) {
			int goUntil = verseCounts[index];
			int startAt = startAtVerses[index];
			for (int verse = startAt; verse < goUntil+1; verse ++) {
				expected.add(new Reference(BookOfBible.Isaiah, chap, verse));
			}
			chap ++;
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		// Mal 3:6-4:6
		refs = this.model.getReferencesForPassage("Mal 3:6-4:6");
		expected = new ArrayList<Reference>();
		
		verseCounts = new int[] { 18, 6 };
		startAtVerses = new int[] { 6, 1 };
		chap = 3;
		for (int index = 0; index < verseCounts.length; index ++) {
			int goUntil = verseCounts[index];
			int startAt = startAtVerses[index];
			for (int verse = startAt; verse < goUntil+1; verse ++) {
				expected.add(new Reference(BookOfBible.Malachi, chap, verse));
			}
			chap ++;
		}
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_wholeBook() {
		ArrayList<Reference> refs = this.model.getReferencesForPassage("1 Kings");
		ArrayList<Reference> expected = this.model.getBookReferences(BookOfBible.Kings1);
		
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		
		refs = this.model.getReferencesForPassage("Philemon");
		expected = this.model.getBookReferences(BookOfBible.Philemon);
		
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_chaptersWeirdSyntax() {
		
		ArrayList<Reference> refs = this.model.getReferencesForPassage("Ephesians 5-6:9");
		ArrayList<Reference> expected = this.model.getChapterReferences(BookOfBible.Ephesians, 5);
		expected.addAll(this.model.getChapterReferences(BookOfBible.Ephesians, 6).subList(0, 9));
		
		assertArrayEquals(expected.toArray(), refs.toArray());
		
		
		refs = this.model.getReferencesForPassage("Hebrews 11-12:2");
		expected = this.model.getChapterReferences(BookOfBible.Hebrews, 11);
		expected.addAll(this.model.getChapterReferences(BookOfBible.Hebrews, 12).subList(0, 2));
		
		assertArrayEquals(expected.toArray(), refs.toArray());
	}
	
	@Test
	public void test_getReferencesForPassage_invalidBookChapterOrVerse() {
		
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Jude 2").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Herman 2").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("John 3:163").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Mal 13:6-24:7").toArray());
		
	}
	
	@Test
	public void test_getReferencesForPassage_invalidInvalidSyntax() {
		
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("1 Tim 3-2").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Deut :2-3").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Josh 6:4- :6").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Ruth :-:").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("2 Sam : 4-7").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("Ephesians 5:2,4").toArray());
		assertArrayEquals(new Reference[] {}, this.model.getReferencesForPassage("John 3;16").toArray());
	}
	
}
